name := "C12-2103-dataset-converter"

version := "0.1-SNAPSHOT"

organization := "tempura"

scalaVersion := "2.10.1"

javaOptions in run += "-Xms5G"

libraryDependencies += "org.sellmerfud" %% "optparse" % "2.0"

libraryDependencies += "com.github.scala-incubator.io" % "scala-io-file_2.10" % "0.4.2"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.0.5"

libraryDependencies += "org.tempura" %% "common-utils" % "0.1-SNAPSHOT" changing()
