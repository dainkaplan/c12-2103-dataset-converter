package org.tempura.c122103dataset
import java.io.File
import scalax.io._

// See readme for usage.
object ReplaceAttributes extends App {

  implicit val codec = scalax.io.Codec.UTF8

  def replaceAttributes(str: String) = {
    var count = 0
    val modified = """coref_class="([^"]+)"""".r
      .replaceAllIn(str, m => {
      count += 1
      s"""coref_set=\"${m.group(1)}\""""
    })
    modified -> count
    //    """(span="([^"]+)")""".r.replaceAllIn(str, m => { /* may have to add min_ids as a dupe of span_words; getting min_words requires parsing XML though. */})
  }

  def replaceContentInFile(f: File) = {
    Resource
      .fromFile(f)
      .lines(includeTerminator = true)
      .foldRight(List[String]() -> 0)((l, accum) => {
      val (m, cnt) = replaceAttributes(l)
      (m :: accum._1) -> (accum._2 + cnt)
    })
  }

  def replaceInFiles(fileOrDir: File, depth: Int = 0) {
    val margin = "\t" * depth
    if (fileOrDir.isDirectory) {
      println(s"${margin}Replacing content in files for ${fileOrDir.getName}...")
      for {
        f <- fileOrDir.listFiles()
      } replaceInFiles(f, depth + 1)
    } else if (fileOrDir.isFile && fileOrDir.getName.endsWith(".xml")) {
      val (lines, count) = replaceContentInFile(fileOrDir)
      if (count > 0) {
        println(s"$margin$count replacements in file ${fileOrDir.getName}")
        fileOrDir.delete()
        for {
          p <- Resource.fromFile(fileOrDir).outputProcessor
          out = p.asOutput
        } out.writeStrings(lines)
      }
    }
  }

  if (args.length != 1) {
    println ("You must provide, as an argument, the base path to the corpus.")
  } else {
    println("WARNING: This is an in-place change to the files in ${args(0)}, do you wish to continue? [Y/N]")
    if (readLine().toLowerCase == "y") {
      replaceInFiles(new File(args(0)))
      println("Done.")
    } else {
      println("Aborted.")
    }
  }
}
