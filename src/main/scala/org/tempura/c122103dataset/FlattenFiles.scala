package org.tempura.c122103dataset

import scala.sys.process._
import java.io.File
import org.sellmerfud.optparse.OptionParser
import org.tempura.console.utils.Ansi

case class Opts(src: List[String], dst: String)

// See readme for usage.
object FlattenFiles extends App {
  def copyFiles(dst: String)(src: String) {
    val files = new File(src).listFiles().filter(_.isDirectory)
    for {
      f <- files
      p = f.getAbsolutePath
      cmd = s"cp -fR $p/ $dst/"
    } {
      println(cmd)
      cmd.!
    }
  }

  def getOpts(args: Array[String]): Either[Opts, String] = try {
    val cfg = new OptionParser[Opts] {
      banner = "%s -s [src/path1,src/path2] -d [dest/path]".format(this.getClass.getName)
      separator("")
      separator("Options:")

      list[String](
        short = "-s",
        long = "--sources",
        info = "path(s) to annotation directories, P, D, or C")((p, cfg) => cfg.copy(src = p))

      reqd[String](
        short = "-d",
        long = "--destination",
        info = "path to output files")((p, cfg) => cfg.copy(dst = p))

    }.parse(args, Opts(List(), ""))
    if (cfg.src.isEmpty || cfg.dst.isEmpty)
      throw new Exception("Source and destination paths are both required.")
    Left(cfg)
  } catch {
    case e: Exception => Right(e.getMessage)
  }

  getOpts(args) match {
    case Left(cfg) => cfg.src.foreach(copyFiles(cfg.dst))
    case Right(err) => println(Ansi.Red %("%s", err))
  }
}